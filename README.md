# Photo-Voltaic Power Sensor
## Power sensor PCB and supporting modules

Components:
- [NodeMCU-32S / GOOUUU-ESP32 development board](http://s.click.aliexpress.com/e/br4MTVvA)
- [M/F pin headers](http://s.click.aliexpress.com/e/cVNbnvMG)
- [2P screw terminals](http://s.click.aliexpress.com/e/KHepCd6)
## PCB Instructions
- Download the gerber files [Gerber_PVMeter.zip](/Gerber_PVMeter.zip)
- Upload and purchase fron your preferred PCB manufacturer (i.e. [JLCPCB](https://jlcpcb.com/))
- Solder in components
## Modify Instructions
- Download source [PVMeter.json](/PVMeter.json)
- Go to [Easyeda](https://easyeda.com) and login/create account
- Create new project and Document->Open->Easyeda Source
## Images
![PCB](/images/PVMeter.png)
